The REST project consists of two applications: manager and worker.


Begin each test by starting up the manager. The command line arguments needed to launch the manager are as follows:

python manager.py [manager's ip address] [manager's port number] [number of workers the manager will expect for this test]

For example:
python manager.py localhost 1000 2


Each worker should then be started. The command line arguments needed to launch each worker are as follows:

python worker.py [worker's ip address] [worker's port number] [manager's ip address] [manager's port number]

For example:
pythong worker.py localhost 2000 localhost 1000


Notes:
1) The manager will not start the test until it has received connections from the number of workers specified in the command line argument used to launch it.

2) If running the manager and workers from the same machine (on different ports), make sure that each one has its own separate copy of the target repository. To make sure this is the case, run each
   Python file in a separate directory.
