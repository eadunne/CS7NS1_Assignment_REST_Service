import git
import os

from flask import Flask, request
import time
import sys

app = Flask(__name__)

# Disable console messages from Flask
import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

REPOSITORY_FOLDER = 'RepositoryDirectory'
WORKERS_NEEDED = None

global manager_ready, workers_joined, next_commit, worker_ports, start_time, end_time, printed_result, all_commits, commit_results, received_results
manager_ready = False
workers_joined = 0
next_commit = 0
worker_ips = []
printed_result = False
received_results = 0

number_of_commits = 0
commits_in_repository = []
commit_results = None

# A worker requests work
@app.route('/request_work/', methods=['GET'])
def request_work():
    global manager_ready, worker_ports, workers_joined, start_time, next_commit
    # Startup procedure:
    # Do not start doing work until a set number of workers have joined
    # This ensures the timing of each test is fair
    if not manager_ready:
        port_number = request.args.get('port_number')
        address = request.args.get('address')
        new_ip = address + ':' + port_number
        global worker_ips
        new_worker = True

        for worker_ip in worker_ips:
            if worker_ip == new_ip:
                new_worker = False

        if new_worker:
            workers_joined += 1
            worker_ips.append(new_ip)
        if workers_joined >= WORKERS_NEEDED:  # Will not start test until certain number of workers have joined
            print('All workers have joined!')
            start_time = time.time()
            print('start time:' + str(start_time))
            manager_ready = True

    if not manager_ready:
        return '1'
    else:
        result = request.args.get('result')
        commit_number = request.args.get('commit_number')
        num_result = float(result)
        global commit_results, received_results
        if num_result > -1: # If this request is actually returning a result, the result will be greater than -1
            new_result = (commit_number, result)
            print('Received commit: ' + str(new_result))
            index_commit_number = int(commit_number)

            if commit_results[index_commit_number] == None: # If this result has never been received before
                received_results += 1
            commit_results[index_commit_number] = new_result    # Record the result
            #commit_results.append(new_result)   # Record the result

            # Go to next commit in repository
            next_commit += 1

        if next_commit < number_of_commits: # If there are more files to process
            commit_tag = commits_in_repository[next_commit]
            #print('Commit tag: ' + str(commit_tag))
            print('Sending commit: ' + str(next_commit) + '/' + str(number_of_commits - 1))
            return str(str(next_commit) + ':' + str(commit_tag))
        else:   # All commits have been processed: the worker is no longer needed
            global printed_result
            if received_results >=  number_of_commits and not printed_result:
                print('All commits processed! Printing results...\n')
                printed_result = True
                end_time = time.time()
                time_difference = end_time - start_time
                for commit_result in commit_results:
                    print (commit_result)
                print('\nTime taken: ' + str(time_difference) + ' seconds')

            return '0'

###################################
# Startup procedure - get files ready
if sys.argv.__len__() < 4:
    print('You have not entered enough parameters. Please follow this format:')
    print('python manager.py <manager\'s ip address> <manager\'s port number> <number of workers the manager will expect for this test>')
    print('See the README file for more information.')
    sys.exit()

print('I am the manager.')
print ('My address is ' + sys.argv[1] + ':' + sys.argv[2])

WORKERS_NEEDED = int(sys.argv[3])

if (WORKERS_NEEDED == 1):
    print('I will wait for', WORKERS_NEEDED, 'worker to join before I begin the test.')
else:
    print('I will wait for', WORKERS_NEEDED, 'workers to join before I begin the test.')

if not os.path.isdir(REPOSITORY_FOLDER):
    print('Cloning repository...')
    directory_to_clone_to = REPOSITORY_FOLDER    # NB) The directory needs to be either empty or non-existent
    git_url_to_clone_from = 'https://github.com/jwyang/faster-rcnn.pytorch.git'  # Test repository
    git.Repo.clone_from(git_url_to_clone_from, directory_to_clone_to)
    print('Repository cloned!')


# Make a list of every commit in the repository
repo = git.Repo(REPOSITORY_FOLDER)
for commit in list(repo.iter_commits()):
    commits_in_repository.append(commit)
    number_of_commits += 1


print('Commits are ready!')
commit_results = [None] * number_of_commits
###################################

if __name__ == '__main__':
    # app.run(debug=True)
    address = sys.argv[1]
    port_number = sys.argv[2]
    app.run(debug=False, host=address, port=int(port_number))