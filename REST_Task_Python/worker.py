import git
import os

from flask import Flask, request
import json
import requests
import sys
import time

from radon.complexity import cc_visit

app = Flask(__name__)

REPOSITORY_FOLDER = 'RepositoryDirectory'

global terminate, repo, g, address, port_number
terminate = False


def calculate_complexity(file):
    try:
        f = open(file, 'r')
        text = f.read()

        complexity = 0
        results = cc_visit(text)  # Calculate the file's cyclomatic complexity using the Radon library
        for result in results:
            complexity += result.complexity  # Add to the total complexity for this commit

        return complexity

    except:
        return 0

# A worker requests work
def request_work(commit_just_processed, result_from_work_just_finished):
    parameters = []
    global address, port_number
    parameters = {'port_number': port_number, 'address': address, 'commit_number': commit_just_processed, 'result': result_from_work_just_finished}

    directory_address = sys.argv[3]
    directory_port_number = sys.argv[4]
    response = requests.get('http://' + directory_address + ':' + directory_port_number + '/request_work/', params=parameters) # Manager runs on port 5000
    if response.text == '0':    # Terminate
        print('Terminating...')
        sys.exit()
    elif response.text == '1':  # Manager is waiting for more workers to join
        time.sleep(1)
        request_work(-1, -1)
    else:
        global g
        commit_number = response.text.split(':')[0]
        commit_tag = response.text.split(':')[1]
        #print('commit number is: ' + commit_number)
        #print('commit tag is: ' + commit_tag)
        g.checkout(commit_tag)

        # Have now checked out the relevant commit - iterate through every file
        total_complexity = 0
        number_of_files = 0
        for path, subdirs, files in os.walk(r'RepositoryDirectory'):
            for filename in files:
                file = os.path.join(path, filename)

                if file.endswith('.py'): # If the file is a python file
                    total_complexity += calculate_complexity(file)

                    number_of_files += 1

        average_for_this_commit = None
        if number_of_files > 0:
            average_for_this_commit = total_complexity / number_of_files
        else:
            average_for_this_commit = 0
        #print('Average cyclomatic complexity for this commit: ' + str(average_for_this_commit))
        request_work(commit_number, average_for_this_commit)  # Send result to manager and request more work



###################################
# Startup procedure

if sys.argv.__len__() < 5:
    print('You have not entered enough parameters. Please follow this format:')
    print('python worker.py <worker\'s ip address> <worker\'s port number> <manager\'s ip address> <manager\'s port number>')
    print('See the README file for more information.')
    sys.exit()

address = sys.argv[1]
port_number = sys.argv[2]

print('I am a worker.')
print ('My address is ' + address + ':' + port_number)
print ('My manager\'s address is ' + sys.argv[3] + ':' + sys.argv[4])

if not os.path.isdir(REPOSITORY_FOLDER):
    print('Cloning repository...')
    directory_to_clone_to = REPOSITORY_FOLDER    # NB) The directory needs to be either empty or non-existent
    git_url_to_clone_from = 'https://github.com/jwyang/faster-rcnn.pytorch.git'  # Test repository
    git.Repo.clone_from(git_url_to_clone_from, directory_to_clone_to)
    print('Repository cloned!')

global repo, g
repo = git.Repo(REPOSITORY_FOLDER)
g = git.Git(REPOSITORY_FOLDER)


# Request work from the manager until it tells the worker to terminate
request_work(-1, -1)

if __name__ == '__main__':
    app.run(debug=False, host=address, port=int(port_number))

